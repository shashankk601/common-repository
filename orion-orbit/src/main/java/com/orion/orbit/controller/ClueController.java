package com.orion.orbit.controller;


import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.orion.orbit.controller.ClueController;
import com.orion.orbit.model.AnsClueMap;
import com.orion.orbit.model.CityCode;
import com.orion.orbit.model.ClueAns;
import com.orion.orbit.model.ClueData;
import com.orion.orbit.model.CountryCode;
import com.orion.orbit.services.ClueServices;

@Controller
@RequestMapping("/newclue")
public class ClueController {

	@Autowired
	ClueServices clueServices;

	//static final Logger logger = Logger.getLogger(ClueController.class);
	private static final Logger logger = Logger.getLogger(ClueController.class.getName());
	@RequestMapping(value = "clueAns/{id}", method = RequestMethod.GET)
	public @ResponseBody
	AnsClueMap getAnsClue(@PathVariable("id") long id) {
		AnsClueMap ansClueMap = null; 
		FileHandler fh;
		
		 logger .debug("Sample debug message"); 
		try {
			fh = new FileHandler("F:/project/MyLogFile");  
			
			 //logger.addHandler(fh);
		       SimpleFormatter formatter = new SimpleFormatter();  
		        fh.setFormatter(formatter);  
		       logger.info("AnsClueMap services implemented successfully..");  
			logger.info("Hello");
			
			ansClueMap= clueServices.getAnsClue(id);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ansClueMap;
	}

	@RequestMapping(value = "clueAns/list", method = RequestMethod.GET)
	public @ResponseBody
	List<AnsClueMap> getAnsClue() {

		List<AnsClueMap> ansClueList = null;
		FileHandler fh;
		
		 logger .debug("Sample debug message"); 
		
		try {
			fh = new FileHandler("F:/project/MyLogFile");  
			
			 //logger.addHandler(fh);
		       SimpleFormatter formatter = new SimpleFormatter();  
		        fh.setFormatter(formatter);  
		       logger.info("AnsClueMap services implemented successfully..");  
			logger.info("Hello");
			ansClueList= clueServices.getAnsClue();
			

		} catch (Exception e) {
			e.printStackTrace();
		}

		return ansClueList;
	}
	@RequestMapping(value = "country/list", method = RequestMethod.GET)
	public @ResponseBody
	List<CountryCode> getCountrycode() {

		List<CountryCode> countryCodeList = null;
		FileHandler fh;
		
		 logger .debug("Sample debug message"); 
		try {
			fh = new FileHandler("F:/project/MyLogFile");  
			
			 //logger.addHandler(fh);
		       SimpleFormatter formatter = new SimpleFormatter();  
		        fh.setFormatter(formatter);  
		       logger.info("AnsClueMap services implemented successfully..");  
			logger.info("Hello");
			countryCodeList = clueServices.getCountry();
			

		} catch (Exception e) {
			e.printStackTrace();
		}

		return countryCodeList;
	}
	@RequestMapping(value = "country/{id}/cities", method = RequestMethod.GET)
	public @ResponseBody
	List<CityCode> getCitiesByCountryCode(@PathVariable("id") long id) {
		List<CityCode> cities = null;
		
		FileHandler fh;
		
		 logger .debug("Sample debug message"); 
		try {
			fh = new FileHandler("F:/project/MyLogFile");  
			
			 //logger.addHandler(fh);
		       SimpleFormatter formatter = new SimpleFormatter();  
		        fh.setFormatter(formatter);  
		       logger.info("AnsClueMap services implemented successfully..");  
			logger.info("Hello");
			cities = clueServices.getCities(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cities;
	}
	@RequestMapping(value = "cities/{id}/clueAnswers", method = RequestMethod.GET)
	public @ResponseBody
	List<ClueAns> getClueanswerByCityCode(@PathVariable("id") long id) {
		List<ClueAns> clueAnswers = null;
		FileHandler fh;
		
		 logger .debug("Sample debug message"); 
		try {
			fh = new FileHandler("F:/project/MyLogFile");  
			
			 //logger.addHandler(fh);
		       SimpleFormatter formatter = new SimpleFormatter();  
		        fh.setFormatter(formatter);  
		       logger.info("AnsClueMap services implemented successfully..");  
			logger.info("Hello");
			clueAnswers = clueServices.getClueAnswers(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return clueAnswers;
	}
	@RequestMapping(value = "/saveAndSubmit", method = RequestMethod.POST)
	public @ResponseBody ClueData saveCluedata(@RequestBody ClueData clueData) {
	
			System.out.println("clue:-" +clueData.getClue() );
			System.out.println("level:-" +clueData.getClueLvl() );
			clueData.setUserId(210);
			//clueData.setClueValidationRuleRuleId(21);
			clueData.setClueTransId(51);
				
				try {
					clueServices.saveClueData(clueData);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			return  clueData;
	 	}
	}
