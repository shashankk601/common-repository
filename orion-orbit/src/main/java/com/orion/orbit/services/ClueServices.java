package com.orion.orbit.services;

import java.util.List;

import com.orion.orbit.model.AnsClueMap;
import com.orion.orbit.model.CityCode;
import com.orion.orbit.model.ClueAns;
import com.orion.orbit.model.ClueData;
import com.orion.orbit.model.CountryCode;


public interface ClueServices {
	
	public List<CountryCode> getCountry() throws Exception;
	public List<CityCode> getCities(long id) throws Exception;
	public List<ClueAns> getClueAnswers(long id) throws Exception;
	public void saveClueData(ClueData clueData)throws Exception;
	public AnsClueMap getAnsClue(long id) throws Exception;
	public List<AnsClueMap> getAnsClue() throws Exception;
	
}
