package com.orion.orbit.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.orion.orbit.dao.ClueDao;
import com.orion.orbit.model.AnsClueMap;
import com.orion.orbit.model.CityCode;
import com.orion.orbit.model.ClueAns;
import com.orion.orbit.model.ClueData;
import com.orion.orbit.model.CountryCode;


public class ClueServicesImpl implements ClueServices {

	@Autowired
	ClueDao clueDao;


	@Override
	public AnsClueMap getAnsClue(long id) throws Exception {
		return clueDao.getAnsClue(id);
	}

	@Override
	public List<AnsClueMap> getAnsClue() throws Exception {
		return clueDao.getAnsClue();
	}

	@Override
	public List<CountryCode> getCountry() throws Exception {
		return clueDao.getCountry();
	}

   @Override
	public List<CityCode> getCities(long id) throws Exception {
		return clueDao.getCities(id);
	}

	@Override
    public List<ClueAns> getClueAnswers(long id) throws Exception {
		return clueDao.getClueAnswers(id);

	}

	@Override
	public void saveClueData(ClueData clueData) throws Exception {
		System.out.println("Inside data service impl");
		clueDao.saveCluedata(clueData);
		
	}
	

}
