package com.orion.orbit.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@Entity
@Table(name = "clue_answer")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ClueAns implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "Ans_ID")
	private long ansId;
	
	@Column(name = "Answer")
	private String ans;
	
	@ManyToOne
	@JoinColumn(name="Answer_CntryID", nullable=false)
	private CountryCode cntryCode;
	
	@ManyToOne
	@JoinColumn(name="Answer_CityID", nullable=false)
	private CityCode cityCode;
	
	@Column(name = "ANSWER_GEO_MAP_Ans_Geo_ID")
	private long geoId;

	@OneToMany(mappedBy="clueAns")
	private Set<AnsClueMap> ansClue;

	public long getAnsid() {
		return ansId;
	}

	public void setAnsId(long ansId) {
		this.ansId = ansId;
	}

	public String getAns() {
		return ans;
	}

	public void setAns(String ans) {
		this.ans = ans;
	}

	public CountryCode getCntryCode() {
		return cntryCode;
	}

	public void setCntryCode(CountryCode cntryCode) {
		this.cntryCode = cntryCode;
	}

	public CityCode getCityCode() {
		return cityCode;
	}

	public void setCityCode(CityCode cityCode) {
		this.cityCode = cityCode;
	}

	public long getGeoId() {
		return geoId;
	}

	public void setGeoId(long geoId) {
		this.geoId = geoId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
}
