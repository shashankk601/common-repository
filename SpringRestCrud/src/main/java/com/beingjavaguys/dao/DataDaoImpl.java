package com.beingjavaguys.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.beingjavaguys.model.AnsClueMap;
import com.beingjavaguys.model.Citycode;
import com.beingjavaguys.model.Cluedata;
import com.beingjavaguys.model.Cluetags;
import com.beingjavaguys.model.Countrycode;

public class DataDaoImpl implements DataDao {

	@Autowired
	SessionFactory sessionFactory;

	Session session = null;
	Transaction tx = null;

	@Override
	public boolean addEntity(Countrycode country_code) throws Exception {

		session = sessionFactory.openSession();
		tx = session.beginTransaction();
		session.save(country_code);
		tx.commit();
		session.close();

		return false;
	}

	@Override
	public Countrycode getEntityById(long id) throws Exception {
		session = sessionFactory.openSession();
		Countrycode country_code = (Countrycode) session.load(Countrycode.class,new Long(id));
		tx = session.getTransaction();
		session.beginTransaction();
		tx.commit();
		return country_code;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Countrycode> getEntityList() throws Exception {
		session = sessionFactory.openSession();
		tx = session.beginTransaction();
		List<Countrycode> country_codeList = session.createCriteria(Countrycode.class)
				.list();
		tx.commit();
		session.close();
		return country_codeList;
	}
	
	@Override
	public boolean deleteEntity(long id)
			throws Exception {
		session = sessionFactory.openSession();
		Object o = session.load(Countrycode.class, id);
		tx = session.getTransaction();
		session.beginTransaction();
		session.delete(o);
		tx.commit();
		return false;
	}
	
	
	@Override
	public Citycode getEntityById1(long id) throws Exception {
		session = sessionFactory.openSession();
		Citycode city_code = (Citycode) session.load(Citycode.class,new Long(id));
		tx = session.getTransaction();
		session.beginTransaction();
		tx.commit();
		return city_code;
	}

	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Citycode> getEntityList1() throws Exception {
		session = sessionFactory.openSession();
		tx = session.beginTransaction();
		List<Citycode> city_codeList = session.createCriteria(Citycode.class)
				.list();
		tx.commit();
		session.close();
		return city_codeList;
	}

	@Override
	public Cluedata getEntityById2(long id) throws Exception {
		session = sessionFactory.openSession();
		Cluedata clue_data = (Cluedata) session.load(Cluedata.class,new Long(id));
		tx = session.getTransaction();
		session.beginTransaction();
		tx.commit();
		return clue_data;
	}

	@Override
	public List<Cluedata> getEntityList2() throws Exception {
		session = sessionFactory.openSession();
		tx = session.beginTransaction();
		List<Cluedata> clue_dataList = session.createCriteria(Cluedata.class)
				.list();
		tx.commit();
		session.close();
		return clue_dataList;
	}

	@Override
	public Cluetags getEntityById3(long id) throws Exception {
		session = sessionFactory.openSession();
		Cluetags clue_tags = (Cluetags) session.load(Cluetags.class,new Long(id));
		tx = session.getTransaction();
		session.beginTransaction();
		tx.commit();
		return clue_tags;
	}

	@Override
	public List<Cluetags> getEntityList3() throws Exception {
		session = sessionFactory.openSession();
		tx = session.beginTransaction();
		List<Cluetags> clue_tagsList = session.createCriteria(Cluetags.class)
				.list();
		tx.commit();
		session.close();
		return clue_tagsList;
	}
	
	

	@Override
	public AnsClueMap getEntityById5(long id) throws Exception {
		session = sessionFactory.openSession();
		AnsClueMap ans_clue_map = (AnsClueMap) session.load(AnsClueMap.class,new Long(id));
		tx = session.getTransaction();
		session.beginTransaction();
		tx.commit();
		return ans_clue_map;
	}

	@Override
	public List<AnsClueMap> getEntityList5() throws Exception {
		session = sessionFactory.openSession();
		tx = session.beginTransaction();
		List<AnsClueMap> ans_clue_list = session.createCriteria(AnsClueMap.class)
				.list();
		tx.commit();
		session.close();
		return ans_clue_list;
	}

	@Override
	public void insertData(Cluedata clue) {
		// TODO Auto-generated method stub
		
	}

}
