package com.beingjavaguys.dao;

import java.util.List;

import org.apache.log4j.Logger;


import com.beingjavaguys.model.AnsClueMap;
import com.beingjavaguys.model.Citycode;
import com.beingjavaguys.model.Cluedata;
import com.beingjavaguys.model.Cluetags;
import com.beingjavaguys.model.Countrycode;



public interface DataDao {

	public boolean addEntity(Countrycode country_code) throws Exception;
	public Countrycode getEntityById(long id) throws Exception;
	public List<Countrycode> getEntityList() throws Exception;
	public boolean deleteEntity(long id) throws Exception;
	
	public Citycode getEntityById1(long id) throws Exception;
	public List<Citycode> getEntityList1() throws Exception;
	
	public Cluedata getEntityById2(long id) throws Exception;
	public List<Cluedata> getEntityList2() throws Exception;
	
	public Cluetags getEntityById3(long id) throws Exception;
	public List<Cluetags> getEntityList3() throws Exception;
	
	
	public AnsClueMap getEntityById5(long id) throws Exception;
	public List<AnsClueMap> getEntityList5() throws Exception;
	void insertData(Cluedata clue);
	
	
}




































































