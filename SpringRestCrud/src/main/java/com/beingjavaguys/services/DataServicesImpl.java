package com.beingjavaguys.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.beingjavaguys.dao.DataDao;
import com.beingjavaguys.model.AnsClueMap;
import com.beingjavaguys.model.Citycode;
import com.beingjavaguys.model.Cluedata;
import com.beingjavaguys.model.Cluetags;
import com.beingjavaguys.model.Countrycode;

public class DataServicesImpl implements DataServices {

	@Autowired
	DataDao dataDao;
	
	@Override
	public boolean addEntity(Countrycode country_code) throws Exception {
		return dataDao.addEntity(country_code);
	}

	@Override
	public Countrycode getEntityById(long id) throws Exception {
		return dataDao.getEntityById(id);
	}

	@Override
	public List<Countrycode> getEntityList() throws Exception {
		return dataDao.getEntityList();
	}

	@Override
	public boolean deleteEntity(long id) throws Exception {
		return dataDao.deleteEntity(id);
	}

	
	@Override
	public Citycode getEntityById1(long id) throws Exception {
		return dataDao.getEntityById1(id);
	}
	
	@Override
	public List<Citycode> getEntityList1() throws Exception {
		return dataDao.getEntityList1();
	}

	@Override
	public Cluedata getEntityById2(long id) throws Exception {
		return dataDao.getEntityById2(id);
	}

	@Override
	public List<Cluedata> getEntityList2() throws Exception {
		return dataDao.getEntityList2();
	}

	@Override
	public Cluetags getEntityById3(long id) throws Exception {
		return dataDao.getEntityById3(id);

	}

	@Override
	public List<Cluetags> getEntityList3() throws Exception {
		return dataDao.getEntityList3();

	}
	
	

	@Override
	public AnsClueMap getEntityById5(long id) throws Exception {
		return dataDao.getEntityById5(id);
	}

	@Override
	public List<AnsClueMap> getEntityList5() throws Exception {
		return dataDao.getEntityList5();
	}
	

}
