package com.beingjavaguys.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;



@Entity
@Table(name = "clue_tags")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Cluetags implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "tag_Id")
	private long tagId;

	@Column(name = "tag")
	private String tag;
	
	@Column(name = "clue_Data_Clue_Id")
	private long clueDataClueId;
	
		
	public long getTagId() {
		return tagId;
	}

	public void setTagId(long cn) {
		this.tagId = tagId;
	}
	

	public String getTag() {
		return tag;
	}

	public void setTag(String firstName) {
		this.tag = tag;
	}
	
	public long getClueDataClueId() {
		return clueDataClueId;
	}

	public void setClueDataClueId(long cn) {
		this.clueDataClueId = clueDataClueId;
	}
	
	
	
	
}
