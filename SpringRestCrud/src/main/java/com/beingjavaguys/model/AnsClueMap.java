package com.beingjavaguys.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@Entity
@Table(name = "answer_clue_mapping")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AnsClueMap implements Serializable {

	private static final long serialVersionUID = 1L;

	
	

	@Id
	@ManyToOne
	@JoinColumn(name="CLUE_DATA_Clue_ID", nullable=false)
	private Cluedata clue_data;
	
	@Id
	@ManyToOne
	@JoinColumn(name="CLUE_ANSWER_Ans_ID", nullable=false)
	private ClueAns clue_ans;
	
	public AnsClueMap() {
		
	}
	
	
	public AnsClueMap(Cluedata clue_data, ClueAns clue_ans) {
		super();
		this.clue_data = clue_data;
		this.clue_ans = clue_ans;
	}


	public ClueAns getClue_ans() {
		return clue_ans;
	}




	public void setClue_ans(ClueAns clue_ans) {
		this.clue_ans = clue_ans;
	}



	public Cluedata getClue_data() {
		return clue_data;
	}




	public void setClue_data(Cluedata clue_data) {
		this.clue_data = clue_data;
	}




	public static long getSerialversionuid() {
		return serialVersionUID;
	}




	
	
	
	

	

	
}
