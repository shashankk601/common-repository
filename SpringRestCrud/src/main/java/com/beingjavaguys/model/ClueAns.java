package com.beingjavaguys.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@Entity
@Table(name = "clue_answer")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ClueAns implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "Ans_ID")
	private long ansid;
	
	@Column(name = "Answer")
	private String ans;
	
	@ManyToOne
	@JoinColumn(name="Answer_CntryID", nullable=false)
	private Countrycode cntrycode;
	
	@ManyToOne
	@JoinColumn(name="Answer_CityID", nullable=false)
	private Citycode citycode;
	
	@Column(name = "ANSWER_GEO_MAP_Ans_Geo_ID")
	private long geoid;

	@OneToMany(mappedBy="clue_ans")
	private Set<AnsClueMap> ans_clue;

	public long getAnsid() {
		return ansid;
	}

	public void setAnsid(long ansid) {
		this.ansid = ansid;
	}

	public String getAns() {
		return ans;
	}

	public void setAns(String ans) {
		this.ans = ans;
	}

	public Countrycode getCntrycode() {
		return cntrycode;
	}

	public void setCntrycode(Countrycode cntrycode) {
		this.cntrycode = cntrycode;
	}

	public Citycode getCitycode() {
		return citycode;
	}

	public void setCitycode(Citycode citycode) {
		this.citycode = citycode;
	}

	public long getGeoid() {
		return geoid;
	}

	public void setGeoid(long geoid) {
		this.geoid = geoid;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
}
