package com.beingjavaguys.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.security.Timestamp;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;



@Entity
@Table(name = "clue_data")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Cluedata implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "clue_Id")
	private long clueId;

	@Column(name = "clue")
	private String clue;
	
	@Column(name = "clue_Desc")
	private String clueDesc;
	
	@Column(name = "clue_Lvl")
	private BigDecimal clueLvl;
	
	@Column(name = "user_Id")
	private long userId;
	
	
	
	@Column(name = "clue_Trans_Id")
	private long clueTransId;
	
	@Column(name = "CD_CREATED_TS")
	private Date CD_CREATED_TS;
	
	/*@Column(name = "CD_UPDATED_TS")
	private Date CD_UPDATED_TS;

	
	@Column(name = "CD_DELETED_TS")
	private Date CD_DELETED_TS;*/
	
	@OneToMany(mappedBy="clue_data")
	private Set<AnsClueMap> ans_clue;
		
	public long getClueId() {
		return clueId;
	}

	public void setId(long clueId) {
		this.clueId = clueId;
	}
	

	public String getClue() {
		return clue;
	}

	public void setClue(String firstName) {
		this.clue = clue;
	}
	
	public String getClueDesc() {
		return clueDesc;
	}

	public void setClueDesc(String firstName) {
		this.clueDesc = clueDesc;
	}
	
	public BigDecimal getClueLvl() {
		return clueLvl;
	}

	public void setClueLvl(BigDecimal clueLvl) {
		this.clueLvl = clueLvl;
	}
	
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	
	public long getClueTransId() {
		return clueTransId;
	}

	public void setClueTransId(long clueTransId) {
		this.clueTransId = clueTransId;
	}
	
	public Date getCD_CREATED_TS() {
		return CD_CREATED_TS;
	}
	
	public void setCD_CREATED_TS(Date cD_CREATED_TS) {
		this.CD_CREATED_TS = cD_CREATED_TS;
	}
	
	
	/*public Date getCD_DELETED_TS() {
	return CD_DELETED_TS;
	}
	public void setCD_DELETED_TS(Date cD_DELETED_TS) {
	this.CD_DELETED_TS = cD_DELETED_TS;
	}
	public Date getCD_UPDATED_TS() {
	return CD_UPDATED_TS;
	}
	public void setCD_UPDATED_TS(Date cD_UPDATED_TS) {
	this.CD_UPDATED_TS = cD_UPDATED_TS;
	}
	*/
}









