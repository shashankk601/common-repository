package com.beingjavaguys.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
@Entity
@Table(name = "city_code")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Citycode implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "city_Code")
	private long cityCode;

	@Column(name = "city_Name")
	private String cityName;
	
	
	

	@OneToMany(mappedBy="citycode")
	private Set<ClueAns> clueans;
	
	//Hibernate requires no-args constructor
		
	
	public long getCityCode() {
		return cityCode;
	}

	public void setCityId(long cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String firstName) {
		this.cityName = cityName;
	}
	
	
	
}
