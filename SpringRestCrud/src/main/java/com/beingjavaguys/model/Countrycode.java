package com.beingjavaguys.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;





@Entity
@Table(name = "country_code")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Countrycode implements Serializable {
	
	private static final long serialVersionUID = 1L;
   
	@Id
	@GeneratedValue
	@Column(name = "cntry_Code")
	private long cntryCode;

	@Column(name = "cntry_Name")
	private String cntryName;
	
	@OneToMany(mappedBy="cntrycode")
	private Set<ClueAns> ansclue;
	

	
	public long getCntryCode() {
		return cntryCode;
	}

	public void setId(long cntryCode) {
		this.cntryCode = cntryCode;
	}
	

	public String getCntryName() {
		return cntryName;
	}

	public void setCntryName(String firstName) {
		this.cntryName = cntryName;
	}
	
	
	
}
