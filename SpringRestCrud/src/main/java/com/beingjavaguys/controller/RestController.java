package com.beingjavaguys.controller;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.SimpleFormatter;

import javax.swing.text.Document;

import org.apache.log4j.Logger;

import com.beingjavaguys.model.AnsClueMap;
import com.beingjavaguys.model.Citycode;
import com.beingjavaguys.model.Cluedata;
import com.beingjavaguys.model.Cluetags;
import com.beingjavaguys.model.Countrycode;
import com.beingjavaguys.model.Status;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beingjavaguys.model.Countrycode;
import com.beingjavaguys.services.DataServices;
import com.beingjavaguys.controller.RestController;
@Controller
@RequestMapping("/newclue")
public class RestController {
	
	@Autowired
	DataServices dataServices;
	//private static final Logger logger = LoggerFactory.getLogger(RestController.class);
	 private static final Logger logger = Logger.getLogger(RestController.class.getName());
//	 Document doc;	
	 
	 @RequestMapping(value = "/country/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	Status addCountrycode(@RequestBody Countrycode country_code) {
		
		try {
			dataServices.addEntity(country_code);
			
			return new Status(1, "Clue  added Successfully !");
		} catch (Exception e) {
			// e.printStackTrace();
			return new Status(0, e.toString());
			
		}

	}
             
	@RequestMapping(value = "country/{id}", method = RequestMethod.GET)
	public @ResponseBody
	Countrycode getCountrycode(@PathVariable("id") long id) {
		Countrycode country_code = null;
		try {
			country_code = dataServices.getEntityById(id);
      
		} catch (Exception e) {
			e.printStackTrace();
		}
		return country_code;
	}

	@RequestMapping(value = "country/list", method = RequestMethod.GET)
	public @ResponseBody
	List<Countrycode> getCountrycode() {

		List<Countrycode> country_codeList = null;
		try {
			country_codeList = dataServices.getEntityList();
			
         // logger.info("Hello");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return country_codeList;
	}

	
	@RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
	public @ResponseBody
	Status deleteCountrycode(@PathVariable("id") long id) {

		try {
			dataServices.deleteEntity(id);
			return new Status(1, "Clue deleted Successfully !");
		} catch (Exception e) {
			return new Status(0, e.toString());
		}

	}
	
	
	
	
	@RequestMapping(value = "city/list", method = RequestMethod.GET)
	public @ResponseBody
	List<Citycode> getCitycode() {

		List<Citycode> city_codeList = null;
		try {
			city_codeList = dataServices.getEntityList1();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return city_codeList;
	}
	
	@RequestMapping(value = "clue_data/{id}", method = RequestMethod.GET)
	public @ResponseBody
	Cluedata getCluedata(@PathVariable("id") long id) {
		Cluedata clue_data = null;
		try {
			clue_data = dataServices.getEntityById2(id);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return clue_data;
	}
	
	
	@RequestMapping(value = "clue_data/list", method = RequestMethod.GET)
	public @ResponseBody
	List<Cluedata> getCluedata() {

		List<Cluedata> clue_dataList = null;
		try {
			clue_dataList = dataServices.getEntityList2();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return clue_dataList;
	}
	
	@RequestMapping(value = "clue_tags/{id}", method = RequestMethod.GET)
	public @ResponseBody
	Cluetags getCluetags(@PathVariable("id") long id) {
		Cluetags clue_tags = null;
		try {
			clue_tags = dataServices.getEntityById3(id);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return clue_tags;
	}
	
	
	@RequestMapping(value = "clue_tags/list", method = RequestMethod.GET)
	public @ResponseBody
	List<Cluetags> getCluetags() {

		List<Cluetags> clue_tagsList = null;
		try {
			clue_tagsList = dataServices.getEntityList3();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return clue_tagsList;
	}
	
	
	
	@RequestMapping(value = "clue_ans/{id}", method = RequestMethod.GET)
	public @ResponseBody
	AnsClueMap getAnsClue(@PathVariable("id") long id) {
		AnsClueMap ans_clue_map = null;
		try {
			ans_clue_map= dataServices.getEntityById5(id);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ans_clue_map;
	}

	@RequestMapping(value = "clue_ans/list", method = RequestMethod.GET)
	public @ResponseBody
	List<AnsClueMap> getAnsClue() {
		//PrintWriter out;
		

		List<AnsClueMap> ans_clue_list = null;
		FileHandler fh;
		
		 logger .debug("Sample debug message"); 
		try {
			 fh = new FileHandler("F:/project/MyLogFile");  
			
			 //logger.addHandler(fh);
		       SimpleFormatter formatter = new SimpleFormatter();  
		        fh.setFormatter(formatter);  
		       logger.info("My first log");  
			logger.info("Hello");
			logger.error("simple error message");
			logger .warn("Sample warn message");
			
			ans_clue_list= dataServices.getEntityList5();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return ans_clue_list;
		
	}

	}
	
			

