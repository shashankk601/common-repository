angular.module('MyClueApp.services', []).
  factory('APIservice', function($http) {

    var API = {};

    API.getClues = function() {
      return $http({
        method: 'JSONP', 
        url: 'http://localhost:8080/SpringRestCrud/newclue/clue_ans/list'
      });
    }

    return API;
  });