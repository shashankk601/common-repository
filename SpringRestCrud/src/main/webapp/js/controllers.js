angular.module('F1FeederApp.controllers', []).
  controller('clueController', function($scope, APIservice) {
    $scope.nameFilter = null;
    $scope.cluesList = [];

    APIservice.getClues().success(function (response) {
        //Dig into the responde to get the relevant data
        $scope.cluesList = response.MRData.StandingsTable.StandingsLists[0].DriverStandings;
    });
  });